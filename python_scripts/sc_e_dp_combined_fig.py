#!/bin/env python3

##
from pylab import *
import glob
import os

c = os.getcwd()

##

dirs = glob.glob('expe*')
print(dirs)
fig, ax = subplots(2, constrained_layout=True, figsize=(10,10))

place =	{
  "000": (0),
  "001": (1)
}

colors = {
    "aas2": "MediumTurquoise",
    "cma1": "LightSeaGreen",
    "jvj1": "red",
    "mmr1": "blue",
    "oga1": "LimeGreen",
    "rhi1": "DarkKhaki",
    "rhi3": "magenta",
    "ssu1": "silver",
    "spr1": "Chocolate"#,
    #"tsa1": "gold"
}

legflag = True

for d in dirs:
    
    os.chdir(d)
    print(d)

    files = glob.glob ('*.csv')
    
    # make namelist
    namelist = []
    for i in files:
        namelist.append(i[:4])
        
    name = files[0][4:8] + ".svg"
    pos = place[name[1:4]]
    
    legflag = True if name[1:4] == '000' else False
    
    # get index of own simulation
    ind = namelist.index('jla1')

    # move entry to back of list
    namelist.append(namelist.pop(ind)) 
    files.append(files.pop(ind))

    # load data
    data_list = []
    for i in files:
        data_list.append(loadtxt(i))
    
    for i, j, k in zip(data_list, files, namelist):
        if k == 'jla1':
            xplot_list = []
            yplot_list = []
            #for m in range(0,len(i)):
                #if(np.logical_and(i[:,1][m] > 0.24, i[:,1][m] < 0.26).all()): 
                #xplot_list.append(i[:,0])
                #yplot_list.append(i[:,1]) 
            ax[pos].plot(i[:,0], i[:,4], '-', color = "black", label = "this\nstudy" if legflag else "", linewidth=4.)
            #print(np.min(i[:,2]))
            print(np.min(i[:,4]))
            a = np.where(i[:,4]==np.min(i[:,4]))
            print( i[:,0][a])
        else:
            xplot_list = []
            yplot_list = []
            #for m in range(0,len(i)):
                #if(np.logical_and(i[:,1][m] > 0.24, i[:,1][m] < 0.26).all()): 
                #xplot_list.append(i[:,0])
                #yplot_list.append(i[:,2]) 
            ax[pos].plot(i[:,0], i[:,4], color = colors[k], label = j[0:4] if legflag else "", linewidth=1.)
            print(np.min(i[:,4]))
            #print(np.max(i[:,4]))
            a = np.where(i[:,4]==np.min(i[:,4]))
            print( i[:,0][a])
    
    if legflag:
        ax[pos].legend(ncol=2)

    ax[pos].set_ylabel('delta p (kPa)')
    ax[pos].set_xlabel('x norm.')

    t = j[5:8].lstrip('0')

    ax[pos].set_title(t)
    
    os.chdir(c)

#show()
#fig.legend( ncol=4 )
fig.savefig('fig_expe_dp_all.svg')
fig.savefig('fig_expe_dp_all.png')
fig.savefig('fig_expe_dp_all.pdf')
